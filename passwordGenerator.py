#!/usr/bin/python3

# Librairies
import argparse
import pyperclip
from random import *
import sys

# Parameters
parser = argparse.ArgumentParser()
parser.add_argument('-l', '--length', type=int, default=12, help="Length - Default : 12")
parser.add_argument('-s', '--special', action="store_true", help="Add special characters")
parser.add_argument('-U', '--uppercase', action="store_true", help="Add uppercase letter")
#parser.add_argument('-L', '--lowercase', action="store_true", help="Add lowercase letter")
parser.add_argument('-n', '--number', action="store_true", help="Add number")
parser.add_argument('-k', '--keyword', type=str, default='', help="Keyword - Default : None | Ex : -k P@ssw0rd")

# Variables
# TL;DR : vars() return the dict of the parameters
vargs = vars(parser.parse_args())
length = vargs['length']
special = vargs['special']
uppercase = vargs['uppercase']
#lowercase = vargs['lowercase']
number = vargs['number']
keyword = vargs['keyword']

nbId = 0
requestedPass = ''
i=0

#tabId={"lowercase":"0","special":"1","uppercase":"2","number":"3","keyword":"4"}
usedId=[0]

# Def
def isKeywordPresent(usedId,k,l):
	if k == '':
		pass
	else:
		# Error case
		if len(k) > l:
			print("[!] Keyword cannot be larger than the desired password.")
			sys.exit(0)
		usedId.append(4)
	return usedId

def add(usedId):
	if special == False:
		pass
	else:
		usedId.append(1)

	if uppercase == False:
		pass
	else:
		usedId.append(2)

	if number == False:
		pass
	else:
		usedId.append(3)

	return usedId

def random(usedId,max):
	a = randint(0, max)
	a = usedId[a]
	return a

def createPass(usedId,id,p,n):
	if id == 0: #default, lowercase
		r = randint(97,122)
		p = p + chr(r)
	if id == 1: #special
		c = choice([(33,47),(58,64),(91,95),(123,126)])
		r = randint(*c)
		p = p + chr(r)
	if id == 2: #uppercase
		r = randint(65,90)
		p = p + chr(r)
	if id == 3: #number
		r = randint(48,57)
		p = p + chr(r)
	if id == 4: #keyword
		p = p + keyword
		usedId.remove(4)
		n = n-1
	return usedId,p,n

# Main
if __name__ == "__main__":
	usedId = isKeywordPresent(usedId,keyword,length)
	usedId = add(usedId)
	nbId = len(usedId)-1
	while i<length:
		charPass = random(usedId,nbId)
		usedId,requestedPass,nbId = createPass(usedId,charPass,requestedPass,nbId)
		i=i+1
	print("~ Password : %s ~" % requestedPass)
	pyperclip.copy(requestedPass)
	print("Password has been copied to the clipboard !")
