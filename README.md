## Synopsis

This tool generates passwords according to the format you specify.

## Motivation

The aim of this projet was to create a local password generator instead of use online one.

## Installation

You should have python3 and pyinstaller module installed to run this script.

```
~$ git clone https://github.com/t0fix/passwordGenerator
~$ pip3 install -r requirements.txt
```

## Help / Usage

### Python
```
~$ python3 passwordGenerator.py -h
usage: passwordGenerator.py [-h] [-l LENGTH] [-s] [-U] [-n] [-k KEYWORD]

optional arguments:
  -h, --help            show this help message and exit
  -l LENGTH, --length LENGTH
                        Length - Default : 12
  -s, --special         Add special characters
  -U, --uppercase       Add uppercase letter
  -n, --number          Add number
  -k KEYWORD, --keyword KEYWORD
                        Keyword - Default : None | Ex : -k P@ssw0rd
```
### Executable
```
C:\Users\Github> .\passwordGenerator.exe -h
usage: passwordGenerator.exe [-h] [-l LENGTH] [-s] [-U] [-n] [-k KEYWORD]

optional arguments:
  -h, --help            show this help message and exit
  -l LENGTH, --length LENGTH
                        Length - Default : 12
  -s, --special         Add special characters
  -U, --uppercase       Add uppercase letter
  -n, --number          Add number
  -k KEYWORD, --keyword KEYWORD
                        Keyword - Default : None | Ex : -k P@ssw0rd
```
## Exemple
### Python
```
~$ python3 passwordGenerator.py -l 23 -U -n -k TestGithub
~ Password : 54ns64cTestGithubJUidQ8wIB002RDL ~
Password has been copied to the clipboard !
```
### Executable
```
C:\Users\Github> .\passwordGenerator.exe -l 12 -s -U
~ Password : faYIpNsGj&Sz ~
Password has been copied to the clipboard !
```
